let s;
let c = [];
let p = function result(inventory,id){
    if(id == null){
        return c;
    }
    if(Array.isArray(inventory) && inventory.length > 0){
            for(i=0;i<inventory.length;i++){
                if(inventory[i].id === id){
                    s = `Car ${inventory[i]['id']} is a ${inventory[i]['car_year']} ${inventory[i]['car_make']} ${inventory[i]['car_model']}`;
                    c.push(s);
                    return c;   
                }
            }
    }  
    else{
        return c;
    }
}

module.exports = p;
